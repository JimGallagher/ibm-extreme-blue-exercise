/** Author: James Gallagher
**/

#include "recipe.h"
#include <string>

recipe::recipe(std::string nName)
{
	name = nName;
}

std::string recipe::getName()
{
	return name;
}

bool recipe::checkIngredients(std::list<std::string> ingredientsToCompare)
{
	// checks to see if the recipes ingredients match with the ingredients entered by user
	if (ingredients.size() != ingredientsToCompare.size())
	{
		//returns false if there not the exact amount
		return false;
	}
	int numOfIngredients = ingredients.size();
	int matchs = 0;

	std::list<std::string>::iterator pos1;
	pos1 = ingredients.begin();

	std::list<std::string>::iterator pos2;
	pos2 = ingredientsToCompare.begin();

	while (pos1 != ingredients.end())
	{
		// cycles through the list and compares ingredients
		while (pos2 != ingredientsToCompare.end())
		{
			if (*pos1 == *pos2)
			{
				matchs++;
			}
			pos2++;
		}
		pos1++;
		pos2 = ingredientsToCompare.begin();
	}
	//checks if all ingredients match and return true
	if (matchs == numOfIngredients)
	{
		return true;
	}
	return false;
}
void recipe::printIngredients()
{
	// prints all ingredients in recipe
	std::list<std::string>::iterator pos;
	pos = ingredients.begin();

	std::cout << "\n" << name << "\n";
	while (pos != ingredients.end())
	{
		std::cout << "\n-" << *pos;
		pos++;
	}
}
void recipe::addIngredient(std::string nIngredient)
{
	// adds ingredients to list.
	std::list<std::string>::iterator pos;
	pos = ingredients.begin();
	bool duplicate = false;

	while (pos != ingredients.end())
	{
		if (*pos == nIngredient)
		{
			// checks for duplicates
			duplicate = true;
		}
		pos++;
	}
	// if no duplicates are found the ingredient is added to the list.
	if (duplicate == false)
	{
		ingredients.push_back(nIngredient);
	}
}