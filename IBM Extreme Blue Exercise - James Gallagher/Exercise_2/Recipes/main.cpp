/** Author: James Gallagher
*	Description: Allows user to enter ingredients and searchs the include recipes for matching ingredients.
*	Test 1: Enter milk, flour, eggs and the ingredients for bread is printed out.
*	Test 2: Enter milk, flour, eggs, sugar and the ingredients for pancakes and cake are printed out.
**/

#include "Recipe.h"
#include <iostream>
#include <string>

recipe* bread;
recipe* cake;
recipe* pancakes;
recipe* scones;

std::list<recipe*> recipesList;

std::list<std::string> addIngredient(std::string nIngredient, std::list<std::string> ingredientList)
{
	// Adds an ingredient to the included list.
	std::list<std::string>::iterator pos;
	pos = ingredientList.begin();
	bool duplicate = false;

	while (pos != ingredientList.end())
	{
		// Checks for duplicates
		if (*pos == nIngredient)
		{
			duplicate = true;
		}
		pos++;
	}
	// adds ingredient if no duplicate are found
	if (duplicate == false)
	{
		ingredientList.push_back(nIngredient);
	}
	//returns list
	return ingredientList;
}

void recipeFinder()
{
	// runs recipe finder in loop until user chooses to quit
	std::cout << "Recipe Finder\n-------------------------\n";
	bool running = true;
	while (running)
	{
		std::list<std::string> ingredientList;
		std::string input;
		while (input != "exit")
		{
			// user enters ingrediet and its added to the lngredient list
			std::cout << "Enter an ingredient or type exit to finish: ";
			std::getline(std::cin, input);
			if (input != "exit")
			{
				ingredientList = addIngredient(input, ingredientList);
			}
		}
		std::list<recipe*>::iterator pos;
		pos = recipesList.begin();

		int foundRecipes = 0;

		// Checks all included recipes for matching ingrdients and prints them
		while (pos != recipesList.end())
		{
			if ((*pos)->checkIngredients(ingredientList))
			{
				std::cout << "\n-------------------------";
				(*pos)->printIngredients();
				foundRecipes++;
			}
			pos++;
		}
		// checks to see if the user wants to check anoter ingredient or quit
		std::cout << "\n------------------------- \n\nRecipes Found: " << foundRecipes << "\nType 1 to Quit or 2 to check for another recipe: ";
		std::getline(std::cin, input);
		if (input == "1")
		{
			running = false;
		}
	}
}

void initiate()
{
	// initiates included recipes and adds them to a recipe list
	bread = new recipe("bread");
	cake = new recipe("cake");
	pancakes = new recipe("pancakes");
	scones = new recipe("scones");

	bread->addIngredient("flour");
	bread->addIngredient("milk");
	bread->addIngredient("eggs");

	cake->addIngredient("flour");
	cake->addIngredient("milk");
	cake->addIngredient("eggs");
	cake->addIngredient("sugar");

	pancakes->addIngredient("flour");
	pancakes->addIngredient("milk");
	pancakes->addIngredient("eggs");
	pancakes->addIngredient("sugar");

	scones->addIngredient("flour");
	scones->addIngredient("milk");
	scones->addIngredient("eggs");
	scones->addIngredient("salt");

	recipesList.push_back(bread);
	recipesList.push_back(cake);
	recipesList.push_back(pancakes);
	recipesList.push_back(scones);
}

int main()
{
	initiate();
	recipeFinder();
	system("pause");
}