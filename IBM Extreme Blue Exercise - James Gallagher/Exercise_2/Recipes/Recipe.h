#ifndef Recipe
#define Recipe
#pragma once

#include <iostream>
#include <list>

class recipe
{
public:
	recipe(std::string nName);

	std::string getName();

	bool checkIngredients(std::list<std::string> ingredientsToCompare);
	void printIngredients();
	void addIngredient(std::string nIngredient);

private:
	std::string name;
	std::list<std::string> ingredients;
};

#endif