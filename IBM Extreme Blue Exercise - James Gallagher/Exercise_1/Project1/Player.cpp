#include "player.h"


player::player(std::string nName, unsigned int nHealth)
{
	health = nHealth;
	name = nName;
}

void player::setName(std::string nName)
{
	name = nName;
}
void player::setHealth(unsigned int nHealth)
{
	health = nHealth;
}

std::string player::getName()
{
	return name;
}
unsigned int player::getHealth()
{
	return health;
}


weapon* player::getWeapon()
{
	return weapon;
}

void player::setWeapon(weapon* nWeapon)
{
	weapon = nWeapon;
}