#ifndef Player
#define Player


#pragma once
#include <iostream>

class player
{
public:
	player(std::string nName, unsigned int nHealth);

	void setName(std::string nName);
	void setHealth(unsigned int nHealth);

	std::string getName();
	unsigned int getHealth();

	level* getLevel();
	void setlevel(level* nLevel);

private:
	std::string name;
	unsigned int health;
	level* level;
};

#endif
