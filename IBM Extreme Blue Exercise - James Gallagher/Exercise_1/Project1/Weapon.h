#ifndef Weapon
#define Weapon

#pragma once
#include <iostream>

class weapon
{
public:
	weapon(std::string nName, unsigned int attack, unsigned int defence);

	void setName(std::string nName);
	void setAttack(unsigned int nAttack);
	void setDef(unsigned int nDef);

	std::string getName();
	unsigned int getAttack();
	unsigned int getDef();

private:
	std::string name;
	unsigned int attack;
	unsigned int defence;

};

#endif