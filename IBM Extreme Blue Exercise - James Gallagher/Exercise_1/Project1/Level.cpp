#include "Level.h"

level::level(unsigned int nLevelNum, level* nNextLevel, std::string nName, unsigned int nAttack, unsigned int nDef) : weapon(nName, nAttack, nDef)
{
	levelNum = nLevelNum;
	nextLevel = nNextLevel;
}

unsigned int level::getLevelNum()
{
	return levelNum;
}
void level::setLevelNum(unsigned int nLevelNum)
{
	levelNum = nLevelNum;
}

level* level::getNextLevel()
{
	return nextLevel;
}
void level::setNextLevel(level* nNextLevel)
{
	nextLevel = nNextLevel;
}