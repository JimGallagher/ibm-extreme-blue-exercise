#include "player.h"
#include "level.h";
#include <iostream>
#include <string>

int main()
{
	level* level6 = new level(6, NULL, "Magic Sword", 10, 9);
	level* level5 = new level(5, level6, "Diamond Sword", 8, 7);
	level* level4 = new level(4, level5, "Iron Sword", 6, 7);
	level* level3 = new level(3, level4, "Stone Sword", 4, 4);
	level* level2 = new level(2, level3, "Wooden Sword", 2, 3);
	level* level1 = new level(1, level2, "CardBoard Sword", 2, 1);

	player* hero = new player("Hero", 100);
	hero->setlevel(level1);

	bool running = true;
	while (running)
	{
		// Player makes Turn

		//...

		// If Player progresses to next level
		{
			// Checks if there is a next Level
			if (hero->getLevel()->getNextLevel() != NULL)
			{
				// Sets next Level
				hero->setlevel(hero->getLevel()->getNextLevel);
			}
			else
			{
				// Ends The game if no level to continue too
				running = false;
				std::cout << "You win!";
			}
		}
		// If player Dies the games ends
		if (hero->getHealth < 0)
		{
			running = false;
			std::cout << "Game Over!";
		}
	}
	system("pause");
}