#ifndef Level
#define Level

#pragma once
#include "Weapon.h"
#include <iostream>

class level : public weapon
{
public:
	level(unsigned int nLevelNum, level* nNextLevel, std::string nName, unsigned int attack, unsigned int defence);

	unsigned int getLevelNum();
	void setLevelNum(unsigned int nLevelNum);

	level* getNextLevel();
	void setNextLevel(level* nNextLevel);

private:
	unsigned int levelNum;
	
	// Pointer to next Level
	level* nextLevel;
};

#endif