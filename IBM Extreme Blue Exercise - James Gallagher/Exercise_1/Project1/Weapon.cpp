#include "weapon.h"

weapon::weapon(std::string nName, unsigned int nAttack, unsigned int nDef)
{
	name = nName;
	attack = nAttack;
	defence = nDef;
}

void weapon::setName(std::string nName)
{
	name = nName;
}
void weapon::setAttack(unsigned int nAttack)
{
	attack = nAttack;
}
void weapon::setDef(unsigned int nDef)
{
	defence = nDef;
}

std::string weapon::getName()
{
	return name;
}
unsigned int weapon::getAttack()
{
	return attack;
}
unsigned int weapon::getDef()
{
	return defence;
}